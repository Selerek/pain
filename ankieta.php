<!DOCTYPE html>
<html>
<head>
    <title>Pytania</title>
</head>
<body>
    <form method="post" action="process.php">
    <?php
$conn = new mysqli("localhost", "root", "", "ankieta");

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT * FROM pytania";
$result = $conn->query($sql);

while ($row = $result->fetch_assoc()) {
    $question_id = $row["id_pytania"];
    $question_text = $row["tresc_pytania"];

    echo "<h2>$question_text</h2>";

    $sql_answers = "SELECT * FROM ankiety_odp
                    WHERE id_odpowiedzi NOT IN (
                        SELECT id_odpowiedzi FROM odpowiedzi WHERE id_pytania = $question_id
                    )";

    $result_answers = $conn->query($sql_answers);

    if ($result_answers !== false && $result_answers->num_rows > 0) {
        $answer_count = 0;
        while ($row_answers = $result_answers->fetch_assoc()) {
            $answer_id = $row_answers["id_odpowiedzi"];
            $answer_text = $row_answers["tresc_odpowiedzi"];

            if ($question_id == 1 && $answer_count < 3) {

                echo "<input type='radio' name='answers[$question_id]' value='$answer_id'>$answer_text<br>";
            } elseif ($question_id == 2 && $answer_count >= 3 && $answer_count < 6) {

                echo "<input type='radio' name='answers[$question_id]' value='$answer_id'>$answer_text<br>";
            } elseif ($question_id == 3 && $answer_count >= 6 && $answer_count < 9) {

                echo "<input type='radio' name='answers[$question_id]' value='$answer_id'>$answer_text<br>";
            }

            $answer_count++;
        }
    }
}

$conn->close();
?>
        <br>
        <input type="submit" value="Submit">
    </form>
    <?php
$conn = new mysqli("localhost", "root", "", "ankieta");

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

if ($_SERVER["REQUEST_METHOD"] === "POST") {
    foreach ($_POST['answers'] as $question_id => $answer_id) {
        $question_id = $conn->real_escape_string($question_id);
        $answer_id = $conn->real_escape_string($answer_id);

        $sql = "INSERT INTO odpowiedzi (id_pytania, id_odpowiedzi) VALUES ('$question_id', '$answer_id')";

        if ($conn->query($sql) !== TRUE) {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
    }

    echo "dodano ";
}

$conn->close();
?>
</body>
</html>
